import './App.css';
import {useEffect} from 'react';
import Header from './Components/LayOut/Header';
import Home from './Components/Pages/Home.js';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import ProductDetails from './Components/Products/ProductDetail'
import PrivateRoute from './Components/Route/ProtectedRoute';

//user imports
import Login from './Components/user/login';
import Register from './Components/user/Register'
import Profile from './Components/user/Profile';
import { loadUser } from './Backend_Linking/UserAction';

import store from './Store';

//cart imports
import Cart from './Components/Cart/Cart';
import Shipping from './Components/Cart/Shipping';
//order imports

//order imports
import ProductList from './Components/admin/ProductList';

//admin imports
import Dashboard from './Components/admin/Dashboard';
import AdminProtectedRoute from './Components/Route/AdminProtectedRoute'
import { useSelector, useDispatch } from 'react-redux';
import NewProduct from './Components/admin/NewProduct';


function App() {
 
  
  return (
    <Router className="App">
      <Header />
      <Routes>
        <Route path = "/" element = {<Home/>}/>
        <Route path = "/product/:id" element = {<ProductDetails/>}/>
        <Route path = "/login" element = {<Login/>}/>
        <Route path = "/register" element = {<Register/>}/>
        <Route path = "/cart" element = {<Cart/>}/>
        
        {/* protected routes */}
        <Route path='/me' element={<PrivateRoute> <Profile /> </PrivateRoute>} />
        <Route path='/shipping' element={<PrivateRoute> <Shipping /> </PrivateRoute>} />

        <Route path='/dashboard' element={<AdminProtectedRoute> <Dashboard /> </AdminProtectedRoute>} />
        <Route path='/admin/products' element={<AdminProtectedRoute> <ProductList /> </AdminProtectedRoute>} />
        <Route path='/admin/addProduct' element={<AdminProtectedRoute> <NewProduct /> </AdminProtectedRoute>} />


      </Routes>
    </Router>
    
  );
}

export default App;
