import axios from 'axios'

import { 
    ALL_PRODUCTS_REQUEST,
    ALL_PRODUCTS_SUCCESS,
    ALL_PRODUCTS_FAIL,
    ADMIN_PRODUCTS_REQUEST,
    ADMIN_PRODUCTS_SUCCESS,
    ADMIN_PRODUCTS_FAIL,
    NEW_PRODUCT_REQUEST,
    NEW_PRODUCT_SUCCESS,
    NEW_PRODUCT_RESET,
    NEW_PRODUCT_FAIL,
    PRODUCT_DETAIL_REQUEST,
    PRODUCT_DETAIL_SUCCESS,
    PRODUCT_DETAIL_FAIL,
    CLEAR_ERRORS

} from '../Constant/productConstant'

//display product
export const displayProducts = () => async (dispatch) => {
    try {
        dispatch({ type: ALL_PRODUCTS_REQUEST })

        const { data } = await axios.get(`${process.env.REACT_APP_API_URL}/product/viewProducts`)

        dispatch({
            type: ALL_PRODUCTS_SUCCESS,
            payload: data
        })
    }catch (error){
        dispatch({
            type: ALL_PRODUCTS_FAIL,
            payload: error.response.data.message
        })
    }
}
//create new product details
export const createProductDetails = (productData) => async (dispatch) => {
    try {
        dispatch({ type: NEW_PRODUCT_REQUEST })

        const config = {
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const { data } = await axios.post(`${process.env.REACT_APP_API_URL}/product/admin/addProduct`, productData, config)
        
        dispatch({
            type: NEW_PRODUCT_SUCCESS,
            payload: data
        })
        console.log(data.response)
    }catch (error){
        console.log(error.response)
        dispatch({
            type: NEW_PRODUCT_FAIL,
            payload: error.response.data.message
        })
    }
}
//display product details
export const displayProductsDetails = (id) => async (dispatch) => {
    try {
        dispatch({ type: PRODUCT_DETAIL_REQUEST })

        const { data } = await axios.get(`${process.env.REACT_APP_API_URL}/product/${id}`)
        

        dispatch({
            type: PRODUCT_DETAIL_SUCCESS,
            payload: data.product
        })
    }catch (error){
        dispatch({
            type: PRODUCT_DETAIL_FAIL,
            payload: error.response.data.message
        })
    }
}

//display product by admin
export const displayAdminProductsDetails = () => async (dispatch) => {
    try {
        dispatch({ type: ADMIN_PRODUCTS_REQUEST })

        const { data } = await axios.get(`${process.env.REACT_APP_API_URL}/product/admin/viewProducts`)
        

        dispatch({
            type: ADMIN_PRODUCTS_SUCCESS,
            payload: data.products
        })
    }catch (error){
        dispatch({
            type: ADMIN_PRODUCTS_FAIL,
            payload: error.response.data.message
        })
    }
}

//clear errors
export const clearError = () => async (dispatch) => {
    dispatch({
        type: CLEAR_ERRORS
    })
}