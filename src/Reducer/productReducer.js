import { 
    ALL_PRODUCTS_REQUEST,
    ALL_PRODUCTS_SUCCESS,
    ALL_PRODUCTS_FAIL,
    PRODUCT_DETAIL_REQUEST,
    PRODUCT_DETAIL_SUCCESS,
    PRODUCT_DETAIL_FAIL,
    ADMIN_PRODUCTS_REQUEST,
    ADMIN_PRODUCTS_SUCCESS,
    ADMIN_PRODUCTS_FAIL,
    NEW_PRODUCT_REQUEST,
    NEW_PRODUCT_SUCCESS,
    NEW_PRODUCT_RESET,
    NEW_PRODUCT_FAIL,
    CLEAR_ERRORS

} from '../Constant/productConstant'
//REDUCER FOR PRODUCT
export const productsReducer = (state = { products: [] }, action) => {
    switch(action.type){
        case ALL_PRODUCTS_REQUEST:
        case ADMIN_PRODUCTS_REQUEST:
            return {
                loading: true,
                products: []
            }
        case ALL_PRODUCTS_SUCCESS:
            return {
                loading: false,
                products: action.payload.products
            }

        case ADMIN_PRODUCTS_SUCCESS:
            return {
                loading: false,
                products: action.payload
            }
            
        case ALL_PRODUCTS_FAIL:
        case ADMIN_PRODUCTS_FAIL:
            return {
                loading: false,
                error: action.payload
            }
        
        case CLEAR_ERRORS:
            return {
                ...state,
                error: null
            }
        default:
            return state;
    }
}
//REDUCER FOR PRODUCT DETAIL
export const productDetailsReducer = (state = {product: {}}, action) => {
    switch(action.type){
        case PRODUCT_DETAIL_REQUEST:
            return{
                ...state,
                loading: true
            }
        case PRODUCT_DETAIL_SUCCESS:
            return{
                loading: false,
                product: action.payload
            }

        case PRODUCT_DETAIL_FAIL:
            return{
                ...state,
                error: action.payload
            }
        case CLEAR_ERRORS:
            return {
                ...state,
                error: null
            }
        default:
            return state
    }
}




//REDUCER FOR NEW PRODUCT DETAIL
export const newProductReducer = (state = {product: {}}, action) => {
    switch(action.type){
        case NEW_PRODUCT_REQUEST:
            return{
                ...state,
                loading: true
            }
        case NEW_PRODUCT_SUCCESS:
            return{
                loading: false,
                success: action.payload.success,
                product: action.payload.product
            }

        case NEW_PRODUCT_FAIL:
            return{
                ...state,
                error: action.payload
            }
        case NEW_PRODUCT_RESET:
            return{
                ...state,
                success: false
            }
        case CLEAR_ERRORS:
            return {
                ...state,
                error: null
            }
        default:
            return state
    }
}