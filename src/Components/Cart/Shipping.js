import React, {Fragment, useState} from 'react'
import { Link, useNavigate, useLocation } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import Loader from '../LayOut/Loader';
import MetaData from '../LayOut/MetaData';

import { saveShippingInfo } from '../../Backend_Linking/CartAction';

const Shipping = () => {
    const { shippingInfo } = useSelector(state => state.cart)

    const [address, setAddress] = useState(shippingInfo.address)
    const [city, setCity] = useState(shippingInfo.city)
    const [phoneNo, setPhoneNo] = useState(shippingInfo.phoneNo)
    const [zipCode, setZipCode] = useState(shippingInfo.zipCode)

    const dispatch = useDispatch()
    const navigate = useNavigate()
    const submitHandler = (e) => {
        e.preventDefault()

        dispatch(saveShippingInfo({address, city, phoneNo, zipCode}))
        navigate('/confirm')
    }

  return (
        <Fragment>
            <MetaData title={'Shipping Info'} />
            <div className="row wrapper">
                <div className="col-10 col-lg-5">
                    <form className="shadow-lg" onSubmit={submitHandler}>
                        <h1 className="mb-4">Shipping Info</h1>
                        <div className="form-group">
                            <label for="address_field">Address</label>
                            <input
                                type="text"
                                id="address_field"
                                className="form-control"
                                value={address}
                                onChange  = {e => setAddress(e.target.value)}
                                required
                            />
                        </div>

                        <div className="form-group">
                            <label for="city_field">City</label>
                            <input
                                type="text"
                                id="city_field"
                                className="form-control"
                                value={city}
                                onChange  = {e => setCity(e.target.value)}
                                required
                            />
                        </div>

                        <div className="form-group">
                            <label htmlFor="phone_field">Phone No</label>
                            <input
                                type="phone"
                                id="phone_field"
                                className="form-control"
                                value={phoneNo}
                                onChange  = {e => setPhoneNo(e.target.value)}
                                required
                            />
                        </div>

                        <div className="form-group">
                            <label for="postal_code_field">Postal Code</label>
                            <input
                                type="number"
                                id="postal_code_field"
                                className="form-control"
                                value={zipCode}
                                onChange  = {e => setZipCode(e.target.value)}
                                required
                            />
                        </div>


                        <button
                            id="shipping_btn"
                            type="submit"
                            className="btn btn-block py-3"
                        >
                            CONTINUE
                            </button>
                    </form>
                </div>
            </div>
        </Fragment>
  )
}

export default Shipping