import React, { Fragment, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useNavigate } from 'react-router-dom'
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Swal from 'sweetalert2'

import Loader from '../LayOut/Loader'
import MetaData from '../LayOut/MetaData';
import { register, clearErrors } from '../../Backend_Linking/UserAction'

const Register = () => {
    // const [user, setUser] = useState({
    //     firstName: '',
    //     lastName: '',
    //     email: '',
    //     password: ''
    // })
    
    //const {firstName, lastName, email, password} = user

    //const [avatar, setAvatar] = useState('');
    //const [avatarPreview, setAvatarPreview] = useState('/images/avatar.jfif');
  
    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')

    const dispatch = useDispatch() 
    const navigate = useNavigate()

    const { isAuth, error, loading, user } = useSelector(state => state.auth)

    useEffect(() => {

        if(isAuth){
            navigate('/')
            Swal.fire({
                title: "Account Created!",
                icon: "success",
                text: `Welcome ${user.firstName.toUpperCase()}, Happy Shopping!`
            })
        }
        if(error){
            navigate('/register')
           Swal.fire({
            title: "ERROR!",
            icon: "error",
            text: "Email already Taken!"
           })
           dispatch(clearErrors())
        }

    }, [dispatch, isAuth, error])

    const submitHandler = (e) => {
        e.preventDefault()

        const formData = new FormData();

        formData.set('firstName', firstName)
        formData.set('lastName', lastName)
        formData.set('email', email)
        formData.set('password', password)
        //formData.set('avatar', avatar)

        dispatch(register(formData))
    }
    
    // const onChange = e => {
    //     if(e.target.name === 'avatar'){
    //         const reader = new FileReader();

    //         reader.onload = () => {
    //             if(reader.readyState === 2){
    //                 setAvatarPreview(reader.result)
    //                 setAvatar(reader.result)
    //             }
    //         }
            
    //         reader.readAsDataURL(e.target.files[0])
            
            
    //     }else {
    //         setUser({
    //             ...user, [e.target.name] : e.target.value
    //         }) 
    //     }
    // }

  return (
    <Fragment>
        <MetaData title={ "Register User" } />
            <div className="row wrapper">
                <div className="col-10 col-lg-5">
                    <form className="shadow-lg" onSubmit={submitHandler} encType='multipart/form-data'>
                        <h1 className="mb-3">Register</h1>

                    <div className="form-group">
                        <label htmlFor="email_field">First Name</label>
                        <input 
                        type="name" 
                        id="name_field1" 
                        className="form-control" 
                        name = 'firstName'
                        value={firstName}
                        onChange = {e =>setFirstName(e.target.value)}
                         />
                    </div>
                    <div className="form-group">
                        <label htmlFor="email_field">Last Name</label>
                        <input 
                        type="name" 
                        id="name_field2" 
                        className="form-control" 
                        name = 'lastName'
                        value={lastName}
                        onChange = {e =>setLastName(e.target.value)} 

                        />
                    </div>

                        <div className="form-group">
                        <label htmlFor="email_field">Email</label>
                        <input
                            type="email"
                            id="email_field"
                            className="form-control"
                            name = 'email'
                            value={email}
                            onChange = {e =>setEmail(e.target.value)}
                        />
                        </div>
            
                        <div className="form-group">
                        <label htmlFor="password_field">Password</label>
                        <input
                            type="password"
                            id="password_field"
                            className="form-control"
                            name = 'password'
                            value={password}
                            onChange = {e =>setPassword(e.target.value)}
                        />
                        </div>

                    {/* <div className='form-group'>
                        <label htmlFor='avatar_upload'>Avatar</label>
                        <div className='d-flex align-items-center'>
                            <div>
                                <figure className='avatar mr-3 item-rtl'>
                                    <img
                                        src={avatarPreview}
                                        className='rounded-circle'
                                        alt='Avatar Preview'
                                    />
                                </figure>
                            </div>
                            <div className='custom-file'>
                                <input
                                    type='file'
                                    name='avatar'
                                    className='custom-file-input'
                                    id='customFile'
                                    accept='images/*'
                                    onChange={onChange}
                                />
                                <label className='custom-file-label' htmlFor='customFile'>
                                    Choose Avatar
                                </label>
                            </div>
                        </div>
                    </div>  */}
            
                        <button
                        id="register_button"
                        type="submit"
                        className="btn btn-block py-3"
                        disabled={ loading ? true : false }
                        >
                        REGISTER
                        </button>
                    </form>
                </div>
            </div>
    </Fragment>
  )
}

export default Register