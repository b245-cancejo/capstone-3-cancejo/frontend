import React, { Fragment, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useNavigate, useLocation, Navigate } from 'react-router-dom'

import Loader from '../LayOut/Loader'
import MetaData from '../LayOut/MetaData';
import { login, clearErrors } from '../../Backend_Linking/UserAction'
import Swal from 'sweetalert2';

const Login = ({location}) => {

    const [email, setEmail] = useState('')
    const[ password, setPassword] = useState('')
  
    const dispatch = useDispatch() 
    const navigate = useNavigate();
    const history = useLocation()
    
    const { isAuth, error, loading, user } = useSelector(state => state.auth)
    
    const redirect = history.search ? history.search.split("=")[1] : '/'
    
    useEffect(() => {

        if(isAuth){
            navigate(history.pathname=redirect)
            Swal.fire({
                title: "Successfully Login!",
                icon: "success",
                text: `Welcome Back ${user.firstName.toUpperCase()}`
            })
        }
        if(error){
            Swal.fire({
                title: "ERROR!",
                icon: "error",
                text: String(error)
               })
            navigate('/login')
            dispatch(clearErrors())
        } 

    }, [dispatch, isAuth, error])

    const submitHandler = (e) => {
        e.preventDefault()

        dispatch(login(email,password))



        // if(user.password ===password && user.email ===email){
        //     dispatch(login(email, password))
            
        // }else{
        //     console.log("wrong email/password")
        // }
        
    }

  return (
    <Fragment>
        {loading ? <Loader /> : (
            <Fragment>
               <MetaData title={'Login'}/> 

                <div className="row wrapper"> 
                    <div className="col-10 col-lg-5">
                        <form className="shadow-lg" onSubmit={submitHandler}>
                            <h1 className="mb-3">Login</h1>
                            <div className="form-group">
                            <label htmlFor="email_field">Email</label>
                            <input
                                type="email"
                                id="email_field"
                                className="form-control"
                                value={email}
                                onChange = {(e) => setEmail(e.target.value)}
                            />
                            </div>
                
                            <div className="form-group">
                            <label htmlFor="password_field">Password</label>
                            <input
                                type="password"
                                id="password_field"
                                className="form-control"
                                value = {password}
                                onChange = {(e) => setPassword(e.target.value)}
                            />
                            </div>

                            <a href="#" className="float-right mt-2">Forgot Password?</a>
                
                            <div>
                                <button 
                                id="login_button"
                                type="submit"
                                className="btn btn-block py-2"
                                >
                                LOGIN
                                </button>
                            </div>

                            <Link to="/register" className="float-right mt-3">Sign up here</Link>
                            

                            
                        </form>
                    </div>
                </div>


            </Fragment>
        )}
    </Fragment>
  )
}

export default Login;