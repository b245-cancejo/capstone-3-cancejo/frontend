import React, { Fragment, useEffect, useState } from 'react'
import MetaData from '../LayOut/MetaData';
import Product from '../Products/product'
import Loader from '../LayOut/Loader';
import { Link } from 'react-router-dom'
import Sidebar from './Sidebar';

import { useDispatch, useSelector } from 'react-redux';
import { createProductDetails, clearError } from '../../Backend_Linking/ProductAction';
import { NEW_PRODUCT_RESET } from '../../Constant/productConstant';


const NewProduct = () => {

    const dispatch = useDispatch()
    
    const [name, setName] = useState('')
    const [price, setPrice] = useState('')
    const [description, setDescription] = useState('')
    const [stocks, setStocks] = useState(0)
    //const [images, setImages] = useState([])
    //const [imagePreview, setImagePreview] = useState([])

    const { loading, error, success} = useSelector(state => state.newProduct)
    useEffect(() => {
      
      if(success){
        History.push('/')
        
        dispatch({type: NEW_PRODUCT_RESET})
      }

    }, [dispatch, error, success])

    const submitHandler = (e) => {
      e.preventDefault()

      const formData = new FormData();

      formData.set('name', name)
      formData.set('price', price)
      formData.set('description', description)
      formData.set('stocks', stocks)
      //formData.set('avatar', avatar)

      dispatch(createProductDetails(formData))
  }
  
  // const onChange = e => {
  //     if(e.target.name === 'avatar'){
  //         const reader = new FileReader();

  //         reader.onload = () => {
  //             if(reader.readyState === 2){
  //                 setAvatarPreview(reader.result)
  //                 setAvatar(reader.result)
  //             }
  //         }
          
  //         reader.readAsDataURL(e.target.files[0])
          
          
  //     }else {
  //         setUser({
  //             ...user, [e.target.name] : e.target.value
  //         }) 
  //     }
  // }

  return (
    <Fragment>
      <MetaData title={'New Products'} />
      <div className='row'>
        <div className='col-12 col-md-2'>
          <Sidebar />
        </div>
        <div className='col-12 col-md-10'>
          <Fragment>
          <div className="wrapper my-5"> 
        <form className="shadow-lg" encType='multipart/form-data' onSubmit={submitHandler
        }>
            <h1 className="mb-4">New Product</h1>

            <div className="form-group">
              <label for="name_field">Name</label>
              <input
                type="text"
                id="name_field"
                className="form-control"
                value={name}
                onChange ={e => setName(e.target.value)}
              />
            </div>

            <div className="form-group">
                <label for="price_field">Price</label>
                <input
                  type="text"
                  id="price_field"
                  className="form-control"
                  value={price}
                onChange ={e => setPrice(e.target.value)}
                />
              </div>

              <div className="form-group">
                <label for="description_field">Description</label>
                <textarea className="form-control" 
                id="description_field" 
                rows="8" 
                value={description}
                onChange ={e => setDescription(e.target.value)}
                ></textarea>
              </div>

              {/* <div className="form-group">
                <label for="category_field">Category</label>
                <select className="form-control" id="category_field">
                    <option>Electronics</option>
                    <option>Home</option>
                    <option>Others</option>
                  </select>
              </div> */}
              <div className="form-group">
                <label for="stock_field">Stock</label>
                <input
                  type="number"
                  id="stock_field"
                  className="form-control"
                  value={stocks}
                  onChange ={e => setStocks(e.target.value)}
                />
              </div>
              
              {/* <div className='form-group'>
                <label>Images</label>
                
                    <div className='custom-file'>
                        <input
                            type='file'
                            name='product_images'
                            className='custom-file-input'
                            id='customFile'
                            multiple
                        />
                        <label className='custom-file-label' for='customFile'>
                            Choose Images
                        </label>
                    </div>
            </div> */}

  
            <button
              id="login_button"
              type="submit"
              className="btn btn-block py-3"
              disabled = {loading ? true : false}
            >
              CREATE
            </button>

          </form>
          </div>
          </Fragment>
        </div>
      </div>
    </Fragment>
  )
}

export default NewProduct