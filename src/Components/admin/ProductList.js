import React, { Fragment, useEffect } from 'react'
import MetaData from '../LayOut/MetaData';
import Product from '../Products/product'
import Loader from '../LayOut/Loader';
import { Link } from 'react-router-dom'
import {useAlert} from 'react-alert'
import { MDBDataTable } from 'mdbreact';

import { useDispatch, useSelector } from 'react-redux';
import { displayAdminProductsDetails, clearError } from '../../Backend_Linking/ProductAction';
import Sidebar from './Sidebar.js'



const ProductList = () => {
  const dispatch = useDispatch() 
  const { loading, error, products } = useSelector(state => state.products)
  
    useEffect(() => {
        dispatch(displayAdminProductsDetails())
    }, [dispatch, error])

    const setProducts = () => {
      const data = {
        columns: [
          {
            label: 'ID',
            field: 'id',
            sort: 'asc'
          },
          {
            label: 'Name',
            field: 'name',
            sort: 'asc'
          },
          {
            label: 'Price',
            field: 'price',
            sort: 'asc'
          },
          {
            label: 'Stocks',
            field: 'stocks',
            sort: 'asc'
          },
          {
            label: 'Actions',
            field: 'actions'
          },
        ],
        rows: []
      }
    
    products.forEach(product => {
      data.rows.push({
        id: product._id,
        name: product.name,
        price: product.price,
        stocks: product.stocks,
        actions: <Fragment>
            <Link to={`/admin/product/${product._id}`} className="btn btn-primary py-1 px-2"><i className='fa fa-eye'></i>
            <i className='fa fa-pencil'></i>
            </Link>
            <button className='btn btn-danger py-1 px-2 ml-2'>
              <i className='fa fa-trash'></i>
            </button>
          </Fragment>
        
      })
    })
    return data;
  }

  return (
    <Fragment>
      <MetaData title={'All Products'} />
      <div className='row'>
        <div className='col-12 col-md-2'>
          <Sidebar />
        </div>
        <div className='col-12 col-md-10'>
          <Fragment>
            <h1 className ="my-5">All Products</h1>
            {loading ? <Loader /> : 
              (<MDBDataTable 
                data = {setProducts()}
                className = "px-3"
                bordered
                striped
                hover
              />)
              }
          </Fragment>
        </div>
      </div>
    </Fragment>
  )
}

export default ProductList