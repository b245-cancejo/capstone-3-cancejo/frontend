import React from 'react';
import { useSelector } from 'react-redux';
import { Navigate } from 'react-router-dom';

const PrivateRoute = ({ children }) => {
    const { isAuth, loading} = useSelector(state => state.auth)

    // If authorized, return an outlet that will render child elements
    // If not, return element that will navigate to login page
    return loading === false && isAuth ? children : <Navigate to="/login" />;
}

export default PrivateRoute