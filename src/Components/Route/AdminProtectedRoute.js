import React from 'react'
import { useSelector } from 'react-redux';
import { Navigate } from 'react-router-dom';

const AdminProtectedRoute = ({ children }) => {
    const { loading, user} = useSelector(state => state.auth)

    // If authorized, return an outlet that will render child elements
    // If not, return element that will navigate to login page
    return loading === false && user.isAdmin === 'true' ? children : <Navigate to="/" />;
}

export default AdminProtectedRoute