import React, { Fragment, useEffect } from 'react'
import MetaData from '../LayOut/MetaData';
import Product from '../Products/product'
import Loader from '../LayOut/Loader';


import { useDispatch, useSelector } from 'react-redux';
import { displayProducts } from '../../Backend_Linking/ProductAction';
const Home = () => {

    const dispatch = useDispatch()
    const { loading, products, error } = useSelector( state => state.products)

    useEffect(() => {
       dispatch(displayProducts()); 
    }, [dispatch])

  return (
    <Fragment>
        {loading ? <Loader />: (
            <Fragment>
                <MetaData title = {"Best Product Online"}/>
                
                    <h1 id="products_heading">Latest Products</h1>

                    <section id="products" className="container mt-5">
                        <div className="row">
                            {products && products.map(product => (
                            <Product key={product._id} product={product} />
                                ))}
            
                        </div>
                    </section>
              
                    
            </Fragment>
        )}
        
    </Fragment>
  )
}

export default Home;