import React, { Fragment } from 'react'
import {Link, useNavigate} from 'react-router-dom';
import { Nav, Navbar, Container, Form, Button} from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import {logout} from '../../Backend_Linking/UserAction'

const Header = () => {
  const { isAuth, user } = useSelector(state => state.auth)
  const dispatch = useDispatch()
  const navigate = useNavigate()

  const logoutHandler = () => {
    
    dispatch(logout())
    navigate('/')
    //place successful message here
    
  }

  return (
    <Fragment>
        <Navbar bg="light" expand="lg">
      <Container fluid>
        <Navbar.Brand href ="/">LazShop</Navbar.Brand>
        <Navbar.Toggle aria-controls="navbarScroll" />
        <Navbar.Collapse id="navbarScroll">
          <Nav
            className="mx-auto my-2 my-lg-0 ml-4"
            style={{ maxHeight: '100px' }}
            navbarScroll
          >
            <Link to="/" className='mx-5'>Home</Link>
            
            
            
            {
              isAuth ? 
              <Fragment>
                <Link to="/cart" className='mx-5'>Cart</Link>
                <Link to="/me" className='mx-5'>My Profile</Link>
                <Link to="/orders/me" className='mx-5'>My Orders</Link>
              </Fragment>
              
              : <Link to="/register" className='mx-5'>Register Now</Link>
            }
            {
              user && user.isAdmin === 'true' && (
                <Link to="/dashboard" className='mx-5'>Dashboard</Link>
              )
            }
            
            {
              isAuth ? <Link onClick={logoutHandler} className='mx-5'>Logout</Link> : <Link to="/login" className='mx-5'>Login</Link>
            }
            
            
          </Nav>
          <Form className="d-flex">
            <Form.Control
              type="search"
              placeholder="Enter Product Name"
              className="me-2"
              aria-label="Search"
            />
            <Button variant="outline-success">Search</Button>
          </Form>
        </Navbar.Collapse>
      </Container>
    </Navbar> 
    </Fragment>
  )
}

export default Header