import { createStore, combineReducers, applyMiddleware } from 'redux';
import * as thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension'
import { productsReducer, productDetailsReducer, newProductReducer } from './Reducer/productReducer'
import { userReducer } from './Reducer/userReducer';
import { cartReducer } from './Reducer/cartReducer';
import { newOrderReducer } from './Reducer/orderReducer';

const reducer = combineReducers({
    products: productsReducer,
    productDetails: productDetailsReducer,
    newProduct: newProductReducer,
    auth: userReducer,
    //newOrder: newOrderReducer,
    cart: cartReducer,

})

let initialState = {
    cart: {
        cartItems: localStorage.getItem('cartItems') 
        ?  JSON.parse(localStorage.getItem('cartItems')) 
        :   [], 
        shippingInfo: localStorage.getItem('shippingInfo') ?
        JSON.parse(localStorage.getItem('shippingInfo'))
        : {}


    }
}


const store = createStore(reducer, initialState, composeWithDevTools(applyMiddleware(thunk.default)))

export default store;